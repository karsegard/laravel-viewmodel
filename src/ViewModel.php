<?php

namespace KDA\Laravel\Viewmodel;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;
use Reflection;
use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Support\Traits\Tappable;

class ViewModel implements Arrayable
{
    use Tappable;
    use Conditionable;

    public function toArray(): array
    {
        return collect((new ReflectionClass($this))->getMethods())
            ->reject(fn (ReflectionMethod $method) =>
                in_array($method->getName(), ['__construct', '__invoke', 'toArray','make','all'])
            )
            ->filter(fn (ReflectionMethod $method) =>
                in_array('public', Reflection::getModifierNames($method->getModifiers()))
            )
            ->mapWithKeys(fn (ReflectionMethod $method) => [
                Str::snake($method->getName()) => $this->{$method->getName()}()
            ])
            ->toArray();
    }

    public function all():Collection
    {
        return collect((new ReflectionClass($this))->getMethods())
        ->reject(fn (ReflectionMethod $method) =>
            strpos($method->getName(),'get') ===false
        )
        ->filter(fn (ReflectionMethod $method) =>
            in_array('public', Reflection::getModifierNames($method->getModifiers()))
        )
        ->mapWithKeys(fn (ReflectionMethod $method) => [
            Str::snake(str_replace('get','',$method->getName())) => $this->{$method->getName()}()
        ]);
    }
}